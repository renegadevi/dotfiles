# Dotfiles
My dotfiles


## Screenshot

### Vim
![screenshot](https://gitlab.com/renegadevi/dotfiles/raw/master/screenshot.png)

### Zsh
![screenshot2](https://gitlab.com/renegadevi/dotfiles/raw/master/screenshot2.png)


## Useful altenrative software to your old commands
https://github.com/sharkdp/bat - A cat(1) clone with wings.

https://github.com/ogham/exa - A modern replacement for ‘ls’.

https://github.com/ajeetdsouza/zoxide - A smarter cd command.

https://github.com/sharkdp/fd - A simple, fast and user-friendly alternative to 'find'

https://github.com/dalance/procs - A modern replacement for ps written in Rust

https://github.com/bootandy/dust - A more intuitive version of du in rust

https://github.com/BurntSushi/ripgrep - ripgrep recursively searches directories for a regex pattern while respecting your gitignore

https://github.com/XAMPPRocky/tokei - Count your code, quickly.

https://github.com/ClementTsang/bottom - Yet another cross-platform graphical process/system monitor.

https://github.com/dbrgn/tealdeer - A very fast implementation of tldr in Rust.

https://github.com/imsnif/bandwhich - Terminal bandwidth utilization tool

https://github.com/pemistahl/grex - A command-line tool and library for generating regular expressions from user-provided test cases

https://github.com/dandavison/delta - A viewer for git and diff output

